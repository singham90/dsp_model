package protos

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/7/4
*@description:
 */
type TrackRequest struct {
	P     string `json:"p" form:"p"`         //加密后pb内容
	Price int64  `json:"price" form:"price"` //结算价格
}
