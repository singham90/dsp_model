package protos

import "gitee.com/singham90/dsp_model/index"

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/20
*@description:
 */
type Diagnosis struct {
	TimeInfoMs map[string]int64 `json:"time_info_ms"`
	Recall     *Recall          `json:"recall"`
	Filter     *Filter          `json:"filter"`
	Rank       *Rank            `json:"rank"`
}
type Rank struct {
	Campaigns []*index.Campaign `json:"campaigns"`
}
type Recall struct {
	CampaignPassNums int64          `json:"campaign_pass_nums"`
	CampaignIds      []uint32       `json:"campaign_ids"`
	RecallDebug      map[int]string `json:"recall_debug"`
}
type Filter struct {
	CampaignPassNums int64                     `json:"campaign_pass_nums"`
	Campaigns        map[int64]*index.Campaign `json:"campaigns"`
}
