package protos

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/20
*@description:
 */
type ProgrammaticRR struct {
	RequestId string       `json:"request_id"` // 唯一id
	UserId    int64        `json:"user_id"`    //用户id
	Timestamp int64        `json:"timestamp"`  // 毫秒。自1970年1月1日0时起的毫秒数
	Request   *BidRequest  `json:"request"`
	Response  *BidResponse `json:"response"`
	Diagnosis *Diagnosis   `json:"diagnosis"`
	//事件
	Event int64 `json:"event"`
	//结算价格
	SettlementPrice int64 `json:"settlement_price"`
}

type TrackingRR struct {
	RequestId string `json:"request_id"` // 唯一id
	UserId    int64  `json:"user_id"`    //用户id
	Timestamp int64  `json:"timestamp"`  // 毫秒。自1970年1月1日0时起的毫秒数
	MediaId   string `json:"media_id"`
	Adid      int64  `json:"adid"`
	//事件
	Event int64 `json:"event"`
	//结算价格
	SettlementPrice int64 `json:"settlement_price"`
}
