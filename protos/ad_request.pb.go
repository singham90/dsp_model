// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.21.12
// source: api/dsp/ad_request.proto

package protos

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type App struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AppName string `protobuf:"bytes,1,opt,name=app_name,json=appName,proto3" json:"app_name,omitempty"`
	PkgName string `protobuf:"bytes,2,opt,name=pkg_name,json=pkgName,proto3" json:"pkg_name,omitempty"`
	Appv    string `protobuf:"bytes,3,opt,name=appv,proto3" json:"appv,omitempty"`
}

func (x *App) Reset() {
	*x = App{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_dsp_ad_request_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *App) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*App) ProtoMessage() {}

func (x *App) ProtoReflect() protoreflect.Message {
	mi := &file_api_dsp_ad_request_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use App.ProtoReflect.Descriptor instead.
func (*App) Descriptor() ([]byte, []int) {
	return file_api_dsp_ad_request_proto_rawDescGZIP(), []int{0}
}

func (x *App) GetAppName() string {
	if x != nil {
		return x.AppName
	}
	return ""
}

func (x *App) GetPkgName() string {
	if x != nil {
		return x.PkgName
	}
	return ""
}

func (x *App) GetAppv() string {
	if x != nil {
		return x.Appv
	}
	return ""
}

type Device struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Ip           string   `protobuf:"bytes,1,opt,name=ip,proto3" json:"ip,omitempty"`
	Province     string   `protobuf:"bytes,2,opt,name=province,proto3" json:"province,omitempty"`
	City         string   `protobuf:"bytes,3,opt,name=city,proto3" json:"city,omitempty"`
	Gender       int32    `protobuf:"varint,4,opt,name=gender,proto3" json:"gender,omitempty"`
	Age          int32    `protobuf:"varint,5,opt,name=age,proto3" json:"age,omitempty"`
	Mac          string   `protobuf:"bytes,6,opt,name=mac,proto3" json:"mac,omitempty"`
	Os           string   `protobuf:"bytes,7,opt,name=os,proto3" json:"os,omitempty"`
	Oaid         string   `protobuf:"bytes,8,opt,name=oaid,proto3" json:"oaid,omitempty"`
	OaidMd5      string   `protobuf:"bytes,9,opt,name=oaid_md5,json=oaidMd5,proto3" json:"oaid_md5,omitempty"`
	AndroidId    string   `protobuf:"bytes,10,opt,name=android_id,json=androidId,proto3" json:"android_id,omitempty"`
	AndroidIdMd5 string   `protobuf:"bytes,11,opt,name=android_id_md5,json=androidIdMd5,proto3" json:"android_id_md5,omitempty"`
	BootMark     string   `protobuf:"bytes,12,opt,name=boot_mark,json=bootMark,proto3" json:"boot_mark,omitempty"`
	UpdateMark   string   `protobuf:"bytes,13,opt,name=update_mark,json=updateMark,proto3" json:"update_mark,omitempty"`
	Ua           string   `protobuf:"bytes,14,opt,name=ua,proto3" json:"ua,omitempty"`
	Model        string   `protobuf:"bytes,15,opt,name=model,proto3" json:"model,omitempty"`
	Brand        string   `protobuf:"bytes,16,opt,name=brand,proto3" json:"brand,omitempty"`
	Osv          string   `protobuf:"bytes,17,opt,name=osv,proto3" json:"osv,omitempty"`
	DevW         int32    `protobuf:"varint,18,opt,name=dev_w,json=devW,proto3" json:"dev_w,omitempty"`
	DevH         int32    `protobuf:"varint,19,opt,name=dev_h,json=devH,proto3" json:"dev_h,omitempty"`
	Dpi          int32    `protobuf:"varint,20,opt,name=dpi,proto3" json:"dpi,omitempty"`
	Density      float32  `protobuf:"fixed32,21,opt,name=density,proto3" json:"density,omitempty"`
	Carrier      string   `protobuf:"bytes,22,opt,name=carrier,proto3" json:"carrier,omitempty"`
	ConnectType  int32    `protobuf:"varint,23,opt,name=connect_type,json=connectType,proto3" json:"connect_type,omitempty"`
	Lat          string   `protobuf:"bytes,24,opt,name=lat,proto3" json:"lat,omitempty"`
	Lon          string   `protobuf:"bytes,25,opt,name=lon,proto3" json:"lon,omitempty"`
	InstallApps  []string `protobuf:"bytes,26,rep,name=install_apps,json=installApps,proto3" json:"install_apps,omitempty"`
}

func (x *Device) Reset() {
	*x = Device{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_dsp_ad_request_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Device) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Device) ProtoMessage() {}

func (x *Device) ProtoReflect() protoreflect.Message {
	mi := &file_api_dsp_ad_request_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Device.ProtoReflect.Descriptor instead.
func (*Device) Descriptor() ([]byte, []int) {
	return file_api_dsp_ad_request_proto_rawDescGZIP(), []int{1}
}

func (x *Device) GetIp() string {
	if x != nil {
		return x.Ip
	}
	return ""
}

func (x *Device) GetProvince() string {
	if x != nil {
		return x.Province
	}
	return ""
}

func (x *Device) GetCity() string {
	if x != nil {
		return x.City
	}
	return ""
}

func (x *Device) GetGender() int32 {
	if x != nil {
		return x.Gender
	}
	return 0
}

func (x *Device) GetAge() int32 {
	if x != nil {
		return x.Age
	}
	return 0
}

func (x *Device) GetMac() string {
	if x != nil {
		return x.Mac
	}
	return ""
}

func (x *Device) GetOs() string {
	if x != nil {
		return x.Os
	}
	return ""
}

func (x *Device) GetOaid() string {
	if x != nil {
		return x.Oaid
	}
	return ""
}

func (x *Device) GetOaidMd5() string {
	if x != nil {
		return x.OaidMd5
	}
	return ""
}

func (x *Device) GetAndroidId() string {
	if x != nil {
		return x.AndroidId
	}
	return ""
}

func (x *Device) GetAndroidIdMd5() string {
	if x != nil {
		return x.AndroidIdMd5
	}
	return ""
}

func (x *Device) GetBootMark() string {
	if x != nil {
		return x.BootMark
	}
	return ""
}

func (x *Device) GetUpdateMark() string {
	if x != nil {
		return x.UpdateMark
	}
	return ""
}

func (x *Device) GetUa() string {
	if x != nil {
		return x.Ua
	}
	return ""
}

func (x *Device) GetModel() string {
	if x != nil {
		return x.Model
	}
	return ""
}

func (x *Device) GetBrand() string {
	if x != nil {
		return x.Brand
	}
	return ""
}

func (x *Device) GetOsv() string {
	if x != nil {
		return x.Osv
	}
	return ""
}

func (x *Device) GetDevW() int32 {
	if x != nil {
		return x.DevW
	}
	return 0
}

func (x *Device) GetDevH() int32 {
	if x != nil {
		return x.DevH
	}
	return 0
}

func (x *Device) GetDpi() int32 {
	if x != nil {
		return x.Dpi
	}
	return 0
}

func (x *Device) GetDensity() float32 {
	if x != nil {
		return x.Density
	}
	return 0
}

func (x *Device) GetCarrier() string {
	if x != nil {
		return x.Carrier
	}
	return ""
}

func (x *Device) GetConnectType() int32 {
	if x != nil {
		return x.ConnectType
	}
	return 0
}

func (x *Device) GetLat() string {
	if x != nil {
		return x.Lat
	}
	return ""
}

func (x *Device) GetLon() string {
	if x != nil {
		return x.Lon
	}
	return ""
}

func (x *Device) GetInstallApps() []string {
	if x != nil {
		return x.InstallApps
	}
	return nil
}

type BidRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	ReqId      string  `protobuf:"bytes,1,opt,name=req_id,json=reqId,proto3" json:"req_id,omitempty"`
	MediaId    string  `protobuf:"bytes,2,opt,name=media_id,json=mediaId,proto3" json:"media_id,omitempty"`
	PositionId string  `protobuf:"bytes,3,opt,name=position_id,json=positionId,proto3" json:"position_id,omitempty"`
	AdType     int32   `protobuf:"varint,4,opt,name=ad_type,json=adType,proto3" json:"ad_type,omitempty"`
	AdStyle    int32   `protobuf:"varint,5,opt,name=ad_style,json=adStyle,proto3" json:"ad_style,omitempty"`
	W          int32   `protobuf:"varint,6,opt,name=w,proto3" json:"w,omitempty"`
	H          int32   `protobuf:"varint,7,opt,name=h,proto3" json:"h,omitempty"`
	BidFloor   int64   `protobuf:"varint,8,opt,name=bid_floor,json=bidFloor,proto3" json:"bid_floor,omitempty"`
	App        *App    `protobuf:"bytes,9,opt,name=app,proto3" json:"app,omitempty"`
	Device     *Device `protobuf:"bytes,10,opt,name=device,proto3" json:"device,omitempty"`
}

func (x *BidRequest) Reset() {
	*x = BidRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_dsp_ad_request_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BidRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BidRequest) ProtoMessage() {}

func (x *BidRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_dsp_ad_request_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BidRequest.ProtoReflect.Descriptor instead.
func (*BidRequest) Descriptor() ([]byte, []int) {
	return file_api_dsp_ad_request_proto_rawDescGZIP(), []int{2}
}

func (x *BidRequest) GetReqId() string {
	if x != nil {
		return x.ReqId
	}
	return ""
}

func (x *BidRequest) GetMediaId() string {
	if x != nil {
		return x.MediaId
	}
	return ""
}

func (x *BidRequest) GetPositionId() string {
	if x != nil {
		return x.PositionId
	}
	return ""
}

func (x *BidRequest) GetAdType() int32 {
	if x != nil {
		return x.AdType
	}
	return 0
}

func (x *BidRequest) GetAdStyle() int32 {
	if x != nil {
		return x.AdStyle
	}
	return 0
}

func (x *BidRequest) GetW() int32 {
	if x != nil {
		return x.W
	}
	return 0
}

func (x *BidRequest) GetH() int32 {
	if x != nil {
		return x.H
	}
	return 0
}

func (x *BidRequest) GetBidFloor() int64 {
	if x != nil {
		return x.BidFloor
	}
	return 0
}

func (x *BidRequest) GetApp() *App {
	if x != nil {
		return x.App
	}
	return nil
}

func (x *BidRequest) GetDevice() *Device {
	if x != nil {
		return x.Device
	}
	return nil
}

var File_api_dsp_ad_request_proto protoreflect.FileDescriptor

var file_api_dsp_ad_request_proto_rawDesc = []byte{
	0x0a, 0x18, 0x61, 0x70, 0x69, 0x2f, 0x64, 0x73, 0x70, 0x2f, 0x61, 0x64, 0x5f, 0x72, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x73, 0x22, 0x4f, 0x0a, 0x03, 0x41, 0x70, 0x70, 0x12, 0x19, 0x0a, 0x08, 0x61, 0x70, 0x70,
	0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x70, 0x70,
	0x4e, 0x61, 0x6d, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x70, 0x6b, 0x67, 0x5f, 0x6e, 0x61, 0x6d, 0x65,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x70, 0x6b, 0x67, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x12, 0x0a, 0x04, 0x61, 0x70, 0x70, 0x76, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x61,
	0x70, 0x70, 0x76, 0x22, 0xee, 0x04, 0x0a, 0x06, 0x44, 0x65, 0x76, 0x69, 0x63, 0x65, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x70, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x70, 0x12, 0x1a,
	0x0a, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x08, 0x70, 0x72, 0x6f, 0x76, 0x69, 0x6e, 0x63, 0x65, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x69,
	0x74, 0x79, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x63, 0x69, 0x74, 0x79, 0x12, 0x16,
	0x0a, 0x06, 0x67, 0x65, 0x6e, 0x64, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06,
	0x67, 0x65, 0x6e, 0x64, 0x65, 0x72, 0x12, 0x10, 0x0a, 0x03, 0x61, 0x67, 0x65, 0x18, 0x05, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x03, 0x61, 0x67, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x6d, 0x61, 0x63, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6d, 0x61, 0x63, 0x12, 0x0e, 0x0a, 0x02, 0x6f, 0x73,
	0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x6f, 0x73, 0x12, 0x12, 0x0a, 0x04, 0x6f, 0x61,
	0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6f, 0x61, 0x69, 0x64, 0x12, 0x19,
	0x0a, 0x08, 0x6f, 0x61, 0x69, 0x64, 0x5f, 0x6d, 0x64, 0x35, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6f, 0x61, 0x69, 0x64, 0x4d, 0x64, 0x35, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x6e, 0x64,
	0x72, 0x6f, 0x69, 0x64, 0x5f, 0x69, 0x64, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61,
	0x6e, 0x64, 0x72, 0x6f, 0x69, 0x64, 0x49, 0x64, 0x12, 0x24, 0x0a, 0x0e, 0x61, 0x6e, 0x64, 0x72,
	0x6f, 0x69, 0x64, 0x5f, 0x69, 0x64, 0x5f, 0x6d, 0x64, 0x35, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0c, 0x61, 0x6e, 0x64, 0x72, 0x6f, 0x69, 0x64, 0x49, 0x64, 0x4d, 0x64, 0x35, 0x12, 0x1b,
	0x0a, 0x09, 0x62, 0x6f, 0x6f, 0x74, 0x5f, 0x6d, 0x61, 0x72, 0x6b, 0x18, 0x0c, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x62, 0x6f, 0x6f, 0x74, 0x4d, 0x61, 0x72, 0x6b, 0x12, 0x1f, 0x0a, 0x0b, 0x75,
	0x70, 0x64, 0x61, 0x74, 0x65, 0x5f, 0x6d, 0x61, 0x72, 0x6b, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x4d, 0x61, 0x72, 0x6b, 0x12, 0x0e, 0x0a, 0x02,
	0x75, 0x61, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x75, 0x61, 0x12, 0x14, 0x0a, 0x05,
	0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6d, 0x6f, 0x64,
	0x65, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x18, 0x10, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x05, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x12, 0x10, 0x0a, 0x03, 0x6f, 0x73, 0x76, 0x18,
	0x11, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6f, 0x73, 0x76, 0x12, 0x13, 0x0a, 0x05, 0x64, 0x65,
	0x76, 0x5f, 0x77, 0x18, 0x12, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04, 0x64, 0x65, 0x76, 0x57, 0x12,
	0x13, 0x0a, 0x05, 0x64, 0x65, 0x76, 0x5f, 0x68, 0x18, 0x13, 0x20, 0x01, 0x28, 0x05, 0x52, 0x04,
	0x64, 0x65, 0x76, 0x48, 0x12, 0x10, 0x0a, 0x03, 0x64, 0x70, 0x69, 0x18, 0x14, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x03, 0x64, 0x70, 0x69, 0x12, 0x18, 0x0a, 0x07, 0x64, 0x65, 0x6e, 0x73, 0x69, 0x74,
	0x79, 0x18, 0x15, 0x20, 0x01, 0x28, 0x02, 0x52, 0x07, 0x64, 0x65, 0x6e, 0x73, 0x69, 0x74, 0x79,
	0x12, 0x18, 0x0a, 0x07, 0x63, 0x61, 0x72, 0x72, 0x69, 0x65, 0x72, 0x18, 0x16, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x07, 0x63, 0x61, 0x72, 0x72, 0x69, 0x65, 0x72, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x6f,
	0x6e, 0x6e, 0x65, 0x63, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x17, 0x20, 0x01, 0x28, 0x05,
	0x52, 0x0b, 0x63, 0x6f, 0x6e, 0x6e, 0x65, 0x63, 0x74, 0x54, 0x79, 0x70, 0x65, 0x12, 0x10, 0x0a,
	0x03, 0x6c, 0x61, 0x74, 0x18, 0x18, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x61, 0x74, 0x12,
	0x10, 0x0a, 0x03, 0x6c, 0x6f, 0x6e, 0x18, 0x19, 0x20, 0x01, 0x28, 0x09, 0x52, 0x03, 0x6c, 0x6f,
	0x6e, 0x12, 0x21, 0x0a, 0x0c, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6c, 0x6c, 0x5f, 0x61, 0x70, 0x70,
	0x73, 0x18, 0x1a, 0x20, 0x03, 0x28, 0x09, 0x52, 0x0b, 0x69, 0x6e, 0x73, 0x74, 0x61, 0x6c, 0x6c,
	0x41, 0x70, 0x70, 0x73, 0x22, 0x93, 0x02, 0x0a, 0x0a, 0x42, 0x69, 0x64, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x12, 0x15, 0x0a, 0x06, 0x72, 0x65, 0x71, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x72, 0x65, 0x71, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6d, 0x65,
	0x64, 0x69, 0x61, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6d, 0x65,
	0x64, 0x69, 0x61, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x70, 0x6f, 0x73, 0x69, 0x74, 0x69, 0x6f,
	0x6e, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x70, 0x6f, 0x73, 0x69,
	0x74, 0x69, 0x6f, 0x6e, 0x49, 0x64, 0x12, 0x17, 0x0a, 0x07, 0x61, 0x64, 0x5f, 0x74, 0x79, 0x70,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x61, 0x64, 0x54, 0x79, 0x70, 0x65, 0x12,
	0x19, 0x0a, 0x08, 0x61, 0x64, 0x5f, 0x73, 0x74, 0x79, 0x6c, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x05, 0x52, 0x07, 0x61, 0x64, 0x53, 0x74, 0x79, 0x6c, 0x65, 0x12, 0x0c, 0x0a, 0x01, 0x77, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x05, 0x52, 0x01, 0x77, 0x12, 0x0c, 0x0a, 0x01, 0x68, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x05, 0x52, 0x01, 0x68, 0x12, 0x1b, 0x0a, 0x09, 0x62, 0x69, 0x64, 0x5f, 0x66, 0x6c,
	0x6f, 0x6f, 0x72, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x08, 0x62, 0x69, 0x64, 0x46, 0x6c,
	0x6f, 0x6f, 0x72, 0x12, 0x1d, 0x0a, 0x03, 0x61, 0x70, 0x70, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x0b, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2e, 0x41, 0x70, 0x70, 0x52, 0x03, 0x61,
	0x70, 0x70, 0x12, 0x26, 0x0a, 0x06, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x18, 0x0a, 0x20, 0x01,
	0x28, 0x0b, 0x32, 0x0e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2e, 0x44, 0x65, 0x76, 0x69,
	0x63, 0x65, 0x52, 0x06, 0x64, 0x65, 0x76, 0x69, 0x63, 0x65, 0x42, 0x0b, 0x50, 0x01, 0x5a, 0x07,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x73, 0x2f, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_api_dsp_ad_request_proto_rawDescOnce sync.Once
	file_api_dsp_ad_request_proto_rawDescData = file_api_dsp_ad_request_proto_rawDesc
)

func file_api_dsp_ad_request_proto_rawDescGZIP() []byte {
	file_api_dsp_ad_request_proto_rawDescOnce.Do(func() {
		file_api_dsp_ad_request_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_dsp_ad_request_proto_rawDescData)
	})
	return file_api_dsp_ad_request_proto_rawDescData
}

var file_api_dsp_ad_request_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_api_dsp_ad_request_proto_goTypes = []interface{}{
	(*App)(nil),        // 0: protos.App
	(*Device)(nil),     // 1: protos.Device
	(*BidRequest)(nil), // 2: protos.BidRequest
}
var file_api_dsp_ad_request_proto_depIdxs = []int32{
	0, // 0: protos.BidRequest.app:type_name -> protos.App
	1, // 1: protos.BidRequest.device:type_name -> protos.Device
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_api_dsp_ad_request_proto_init() }
func file_api_dsp_ad_request_proto_init() {
	if File_api_dsp_ad_request_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_dsp_ad_request_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*App); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_dsp_ad_request_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Device); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_dsp_ad_request_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BidRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_dsp_ad_request_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_api_dsp_ad_request_proto_goTypes,
		DependencyIndexes: file_api_dsp_ad_request_proto_depIdxs,
		MessageInfos:      file_api_dsp_ad_request_proto_msgTypes,
	}.Build()
	File_api_dsp_ad_request_proto = out.File
	file_api_dsp_ad_request_proto_rawDesc = nil
	file_api_dsp_ad_request_proto_goTypes = nil
	file_api_dsp_ad_request_proto_depIdxs = nil
}
