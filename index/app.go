package index

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/7/11
*@description:
 */
type App struct {
	Id   int64  `json:"-" gorm:"column:id;primaryKey;autoIncrement;not null;comment:主键"`
	Name string `json:"name" gorm:"column:name;type:varchar(200);not null;default:'';comment:名称"`
	Key  string `json:"key" gorm:"-"`
}
