package index

import (
	"time"
)

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/19
*@description:
 */
type CampaignData struct {
	//点击率
	Ctr        string    `json:"ctr" gorm:"-"`
	CampaignId int64     `json:"campaign_id" gorm:"column:campaign_id;type:bigint(20);not null;comment:广告计划id;primaryKey;index:index_campaign_id;index:idx_campaign_data"`
	Date       time.Time `json:"-" gorm:"column:date;type:date;not null;comment:日期;primaryKey;index:index_date;index:idx_campaign_data;index:idx_data_media_id"`
	MediaID    string    `json:"media_id,omitempty"  gorm:"column:media_id;type:varchar(20);default:'';not null;comment:媒体ID;primaryKey;index:index_media_id;index:idx_data_media_id"`
	DateStr    string    `json:"date_str" gorm:"-"`
	Burnt      float64   `json:"burnt" gorm:"column:burnt;type:decimal(10,2);not null;comment:消耗"`
	Fill       int64     `json:"fill" gorm:"column:fill;type:bigint(20);not null;comment:填充数"`
	Impression int64     `json:"impression" gorm:"column:impression;type:bigint(20);not null;comment:展示"`
	Click      int64     `json:"click" gorm:"column:click;type:bigint(20);not null;comment:点击"`
	Awaken     int64     `json:"awaken" gorm:"column:awaken;type:bigint(20);not null;comment:唤起"`
	_          struct{}  `gorm:"uniqueIndex:idx_campaign_data_campaign_id_date"`
}
type SelectCampaignData struct {
	CampaignData
	CampaignDataSuper
}
type CampaignDataSuper struct {
	// 以下字段为了方便查询,campaign表中的字段
	Name    string `json:"name" gorm:"column:name;type:varchar(200);not null;default:'';comment:名称"`
	Os      string `json:"os" gorm:"column:os;type:varchar(20);not null;default:0;comment:系统类型,ios/android"`
	AdType  int    `json:"ad_type" gorm:"column:ad_type;type:int(1);not null;default:0;comment:广告类型1 :横幅广告、 2 :开屏广告、 3 :插屏广 告 4 :激励视频、 5 :信息流广告"`
	AdStyle int    `json:"ad_style" gorm:"column:ad_style;type:int(1);comment:广告形式1 :视频广告(开屏、信息流)2 :图片(横幅、 插屏、 开屏)广告3 : 单图上文(信息流)4:3图上文"`
}

// 设置表名
func (CampaignDataSuper) TableName() string {
	return "campaign"
}

// 设置表名
func (CampaignData) TableName() string {
	return "campaign_data"
}
