package index

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/19
*@description:
 */

type UpdateNotify_EntityType int32

const (
	UpdateNotify_Table_Campaign    UpdateNotify_EntityType = 1 //活动推广表
	UpdateNotify_INVERTED_Campaign UpdateNotify_EntityType = 2 //活动推广倒排索引
)

type Campaign_Status int32

const (
	//0:审核通过，待启动 1:投放中， 4:已暂停
	Campaign_Status_UNKNOWN Campaign_Status = 0
	Campaign_Status_Active  Campaign_Status = 1
	Campaign_Status_Delete  Campaign_Status = 3
	Campaign_Status_Stop    Campaign_Status = 4
)
