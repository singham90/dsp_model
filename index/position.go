package index

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/7/11
*@description:
 */
type Position struct {
	Id        int64  `json:"id" gorm:"column:id;primaryKey;autoIncrement;not null;comment:主键"`
	Name      string `json:"label" gorm:"column:name;type:varchar(200);not null;default:'';comment:名称"`
	MediaId   int64  `json:"-" gorm:"column:media_id;type:bigint(20);not null;comment:媒体id;index:index_media_id;index:idx_position_media_id"`
	MediaName string `json:"media_name"`
}

// TableName 表名
func (Position) TableName() string {
	return "position"
}
