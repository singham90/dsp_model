package index

import "time"

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/18
*@description:
 */
type User struct {
	//	主键
	Id       int64  `json:"id" gorm:"column:id;primaryKey;autoIncrement;not null;comment:主键"`
	UserName string `json:"user_name" gorm:"column:user_name;type:varchar(20);not null;default:'';comment:用户名"`
	Password string `json:"password,omitempty" gorm:"column:password;type:varchar(20);not null;default:'';comment:密码"`
	//	状态 0:正常 1:禁用 2:删除
	Status   int       `json:"status" gorm:"column:status;type:int(1);not null;default:0;comment:状态"`
	CreateAt time.Time `json:"create_at" gorm:"autoCreateTime;column:create_at"` //创建时间
	UpdateAt time.Time `json:"update_at" gorm:"autoUpdateTime;column:update_at"` //更新时间
}

// 设置表名
func (User) TableName() string {
	return "user"
}
