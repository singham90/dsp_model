package index

import (
	"time"
)

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/18
*@description:
 */

type ListCampaign struct {
	Campaign
	CampaignSuper
}
type CampaignSuper struct {
	Ctr        string  `json:"ctr" gorm:"-"`
	Burnt      float64 `json:"burnt" gorm:"column:burnt;type:decimal(10,2);not null;comment:消耗"`
	Impression int64   `json:"impression" gorm:"column:impression;type:bigint(20);not null;comment:展示"`
	Click      int64   `json:"click" gorm:"column:click;type:bigint(20);not null;comment:点击"`
	Awaken     int64   `json:"awaken" gorm:"column:awaken;type:bigint(20);not null;comment:唤起"`
}

// Campaign 广告计划
type Campaign struct {
	Id               int64      `json:"id" gorm:"column:id;primaryKey;autoIncrement;not null;comment:主键"`
	UserId           int64      `json:"user_id" gorm:"column:user_id;type:bigint(20);not null;default:0;comment:用户id"`
	Os               string     `json:"os" gorm:"column:os;type:varchar(20);not null;default:0;comment:系统类型,ios/android"`
	AdType           int        ` json:"ad_type" gorm:"column:ad_type;type:int(1);not null;default:0;comment:广告类型1 :横幅广告、 2 :开屏广告、 3 :插屏广 告 4 :激励视频、 5 :信息流广告"`
	AdStyle          int        ` json:"ad_style" gorm:"column:ad_style;type:int(1);comment:广告形式1 :视频广告(开屏、信息流)2 :图片(横幅、 插屏、 开屏)广告3 : 单图上文(信息流)4:3图上文"`
	AdSize           string     ` json:"ad_size" gorm:"column:ad_size;type:varchar(50);comment:广告尺寸"`
	Name             string     `json:"name" gorm:"column:name;type:varchar(200);not null;default:'';comment:名称"`
	StartTime        *time.Time `json:"start_time" gorm:"column:start_time;type:datetime;comment:开始时间"`
	EndTime          *time.Time `json:"end_time" gorm:"column:end_time;type:datetime;comment:结束时间"`
	Title            string     `json:"title" gorm:"column:title;type:varchar(200);not null;default:'';comment:广告title"`
	Desc             string     ` json:"desc" gorm:"column:desc;type:varchar(200);not null;default:'';comment:广告描述"`
	ImageUrls        string     `json:"image_urls" gorm:"column:image_urls;type:text;comment:广告图片，逗号间隔"`
	ClickUrl         string     `json:"click_url" gorm:"column:click_url;type:text;comment:点击检测连接，逗号间隔"`
	DeeplinkUrl      string     `json:"deeplink_url" gorm:"column:deeplink_url;type:text;comment:deeplink_url"`
	MediaType        string     `json:"media_type" gorm:"column:media_type;type:varchar(20);comment:媒体类型数组，逗号间隔"`
	MediaPositionIds string     `json:"media_position_ids" gorm:"column:media_position_ids;type:text;comment:媒体广告位id数组，json数组"`
	InstallApps      string     `json:"install_apps" gorm:"column:install_apps;type:varchar(200);not null;comment:安装app包名，逗号间隔"`
	Area             string     `json:"area" gorm:"column:area;type:text;comment:地域定向，逗号间隔"`
	Gender           int        `json:"gender" gorm:"column:gender;type:int(1);not null;default:0;comment:性别,0不限，1男，2女"`
	Age              string     `json:"age" gorm:"column:age;type:varchar(200);default:ALL;comment:年龄，1,8岁以下；2，18-25岁；3，25-35岁；4,35-45；5,45-60岁，6,60以上，逗号间隔。默认ALL"`
	Price            float64    `json:"price" gorm:"column:price;type:decimal(10,2);not null;default:0;comment:出价 分"`
	Budget           float64    `json:"budget" gorm:"column:budget;type:decimal(10,2);not null;default:0;comment:日预算分"`
	TotalBudget      float64    `json:"total_budget" gorm:"column:total_budget;type:decimal(10,2);not null;default:0;comment:总预算分"`
	PromotionTime    string     `json:"promotion_time" gorm:"column:promotion_time;type:text;comment:推广时间数组时间段定向- 一周 24 * 7 位数字字符表示 ，1投放0不投放； 24小时 111111111111111111111111 7个24小时逗号隔开一周 24 * 7 位数字字符表示 ，1投放0不投放； 24小时 111111111111111111111111 7个24小时逗号隔开"`
	CreateAt         time.Time  `json:"create_at" gorm:"autoCreateTime;column:create_at"` //创建时间
	UpdateAt         time.Time  `json:"update_at" gorm:"autoUpdateTime;column:update_at"` //更新时间
	//状态  0:审核通过，待启动 1:投放中，3、已删除 4:已暂停
	Status             Campaign_Status     `json:"status" gorm:"column:status;type:int(1);not null;default:0;comment:状态  0:审核通过，待启动 1:投放中，3、已删除 4:已暂停  "`
	MediaPositionInfos []MediaPositionInfo `json:"media_position_infos" gorm:"-"`
}
type MediaPositionInfo struct {
	PositionId string `json:"position_name"`
	MediaId    string `json:"media_name"`
}

// TableName 表名
func (Campaign) TableName() string {
	return "campaign"
}

// get status
func (c *Campaign) GetStatus() Campaign_Status {
	return c.Status
}

// get id
func (c *Campaign) GetId() int64 {
	return c.Id

}
