package index

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/6/19
*@description:
 */
type UpdateNotify struct {
	Transaction *UpdateNotify_Transaction `json:"transaction" gorm:"column:transaction"`
}

type UpdateNotify_OperationType int32

const (
	UpdateNotify_insert UpdateNotify_OperationType = 0
	UpdateNotify_Update UpdateNotify_OperationType = 1
)

type UpdateNotify_Transaction struct {
	Type          UpdateNotify_EntityType    `json:"type" gorm:"column:type"`                     // 更新的实体类型
	Size          int32                      ` json:"size" gorm:"column:size"`                    // 数据keys的大小
	Keys          []string                   ` json:"keys" gorm:"column:keys"`                    // keys。如果是正排表，key就是实体的ID；如果是倒排表，key为倒排项（词）
	Message       string                     ` json:"message" gorm:"column:message"`              // 实体内容，主服务向后台推送数据时使用
	Ids           []string                   ` json:"ids" gorm:"column:ids"`                      // ids。如果是删除操作，则这里有值，方便引擎删除内存
	OperationType UpdateNotify_OperationType `json:"operation_type" gorm:"column:operation_type"` // 操作类型
}
