package index

/**
*@authoer:singham<chenxiao.zhao>
*@createDate:2023/7/11
*@description:
 */
type MediaPosition struct {
	Media
	Children []*Position `json:"children" gorm:"foreignKey:MediaId;references:Id"`
}
type Media struct {
	Id   int64  `json:"id" gorm:"column:id;primaryKey;autoIncrement;not null;comment:主键"`
	Name string `json:"label" gorm:"column:name;type:varchar(200);not null;default:'';comment:名称"`
}

// TableName 表名
func (Media) TableName() string {
	return "media"
}
